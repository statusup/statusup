import { Component, OnInit } from '@angular/core';
import { ComponentsService, StorageService } from 'src/app/services';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  dados = {
    email: "",
    senha: "",
  }

  password: boolean = true;

  constructor(
    public components: ComponentsService,
    private router: Router,
    private storage: StorageService,
  ) { 
    this.components.isHeaderVisible = false;
    this.components.isSidebarVisible = false;
  }

  ngOnInit(): void {
  }
  
  ngOnDestroy() {
    this.components.isHeaderVisible = true;
    this.components.isSidebarVisible = true;
  }

  login() {
    if(this.dados.email == 'teste@gmail.com' && this.dados.senha == '123'){
      this.storage.setItem('login', this.dados);
      this.router.navigate(['adm']);
    } else {
      this.components.createToastr('warning', 'Login ou senha inválidos.');
    }
  }

}