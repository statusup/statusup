import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setItem(name, value) {
    value = typeof value == "object" ? JSON.stringify(value) : value;
    return localStorage.setItem(name, value);
  }

  getItem(name) {
    let data = localStorage.getItem(name);
    let info = {};
    try { info = JSON.parse(data); } catch (error) { info = data; };
    return info;
  }

  getKeys() {
    let keys = [];
    for (var i = 0; i < localStorage.length; i++) {
      keys.push(localStorage.key(i));
    }
    return keys;
  }

  removeItem(name) {
    return localStorage.removeItem(name);
  }

  clearStorage() {
    return localStorage.clear();
  }
}
