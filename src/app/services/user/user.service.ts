import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userData: any = null;

  constructor(
    private storage: StorageService
  ) { }

  insertUser(data){
    this.storage.setItem('login', data);
    this.userData = data;
  }

  searchUser(){   
    this.userData = this.storage.getItem('login');
  }

  logout(){
    this.storage.removeItem('login');
    this.userData = null;
  }
}
