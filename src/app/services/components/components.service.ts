import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  isHeaderVisible: boolean = true;
  isSidebarVisible: boolean = true;
  
  constructor(
    private toastr: ToastrService
  ) { }

  createToastr(type: 'success' | 'error' | 'info' | 'warning', message: string, title: string = ""){
    this.toastr[type](message, title);
  }
}
