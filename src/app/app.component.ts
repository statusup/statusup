import { Component } from '@angular/core';
import { ComponentsService, StorageService, UserService } from './services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  pages = [
    { title: "Home", url: "", icon: "fas fa-home", class: "hover-blue" },
    { title: "Usuarios", url: "", icon: "fas fa-user", class: "" },
    { title: "Marcas", url: "", icon: "fab fa-buffer", class: "" },
    { title: "Gerenciar OS", url: "", icon: "fas fa-list-ul", class: "" },
  ]

  isSidebarActive: boolean = false;

  constructor(
    public components: ComponentsService,
    private route: ActivatedRoute,
    private router: Router,
    private storage: StorageService,
    private user: UserService
  ) {
    if (window.innerWidth > 767) {
      this.isSidebarActive = true;
    }
    this.initialize();
  }

  initialize() {
    this.user.searchUser();
    if(!this.user.userData){
      this.router.navigate(['login']);
    } else {
      this.router.navigate(['adm']);
    }
  }

  logOut() {
    this.user.logout();
    this.router.navigate(['login']);
  }
}
